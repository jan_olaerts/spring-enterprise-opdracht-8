function getMessage() {
    const messageId = document.querySelector("#messageId").value;
    const xhr = new XMLHttpRequest();
    xhr.onload = loadMessage;
    xhr.open("GET", "http://localhost:8080/messages/" + messageId, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(null);
    console.log('Send Get Request')
}

function loadMessage() {
    let messageText = "";

    console.log(this);
    switch(this.status) {
        case 200:
            const message = JSON.parse(this.responseText);
            messageText = message.author + " : " + message.text;
            break;
        case 404:
            messageText = "Message not found";
            break;
        default:
            messageText = "Error. Status: " + status;
    }

    document.querySelector("#messageField").innerText = messageText;
    document.querySelector("#messageId").value = "";
}

function postMessage(e) {
    e.preventDefault();

    let authorField = document.querySelector("#postAuthor");
    let textField = document.querySelector("#postText");
    const message = {
        "author": authorField.value,
        "text": textField.value
    };

    const xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:8080/messages", true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept", "*");
    xhr.send(JSON.stringify(message));

    authorField.value = "";
    textField.value = "";
}

function putMessage() {

    let idField = document.querySelector("#putId");
    let authorField = document.querySelector("#putAuthor");
    let textField = document.querySelector("#putText");

    const message = {
        "id": idField.value,
        "author": authorField.value,
        "text": textField.value
    }

    const xhr = new XMLHttpRequest();
    xhr.open("PUT", "http://localhost:8080/messages/" + message.id, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept", "*");
    xhr.send(JSON.stringify(message));

    idField.value = "";
    authorField.value = "";
    textField.value = "";
}

function deleteMessage() {

    let idField = document.querySelector("#deleteId");
    const id = idField.value;

    const xhr = new XMLHttpRequest();
    xhr.open("DELETE", "http://localhost:8080/messages/" + id, true);
    xhr.send(null);

    idField.value = "";
}

function init() {
    const getButton = document.querySelector("#getButton");
    const postButton = document.querySelector("#postButton");
    const putButton = document.querySelector("#putButton");
    const deleteButton = document.querySelector("#deleteButton");

    getButton.addEventListener("click", getMessage);
    postButton.addEventListener("click", postMessage);
    putButton.addEventListener("click", putMessage);
    deleteButton.addEventListener("click", deleteMessage);
}

window.addEventListener("load", init);