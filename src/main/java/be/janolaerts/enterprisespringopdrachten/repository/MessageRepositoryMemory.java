package be.janolaerts.enterprisespringopdrachten.repository;

import be.janolaerts.enterprisespringopdrachten.domain.Message;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Repository
@CrossOrigin(origins="*")
public class MessageRepositoryMemory implements MessageRepository {

    private Map<Integer, Message> messages = new HashMap<>();

    @PostConstruct
    public void init() {
        Message message1 = new Message(1, "Homer", "Hello");
        Message message2 = new Message(2, "Bart", "Ciao");

        messages.put(message1.getId(), message1);
        messages.put(message2.getId(), message2);
    }

    @Override
    public synchronized Message getMessageById(int id) {
        return messages.get(id);
    }

    @Override
    public synchronized List<Message> getAllMessages() {
        return new ArrayList<>(messages.values());
    }

    @Override
    public synchronized List<Message> getMessagesByAuthor(String author) {
        return messages.values().stream()
                .filter(m -> m.getAuthor().equals(author))
                .collect(Collectors.toList());
    }

    @Override
    public synchronized Message createMessage(Message message) {

        if(message.getId() == 0) message.setId(createId());

        messages.put(message.getId(), message);
        return message;
    }

    @Override
    public synchronized Message updateMessage(Message message) {

        messages.put(message.getId(), message);
        return message;
    }

    @Override
    public synchronized void deleteMessage(int id) {
        messages.remove(id);
    }

    private int createId() {
        OptionalInt max = messages.keySet().stream()
                .mapToInt(Integer::intValue).max();
        return max.isEmpty() ? 1 : max.getAsInt() +1;
    }
}