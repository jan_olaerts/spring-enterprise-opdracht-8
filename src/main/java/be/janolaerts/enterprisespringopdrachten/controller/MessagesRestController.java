package be.janolaerts.enterprisespringopdrachten.controller;

import be.janolaerts.enterprisespringopdrachten.domain.Message;
import be.janolaerts.enterprisespringopdrachten.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping(value="/messages")
//@CrossOrigin(origins="*")
public class MessagesRestController {

    @Autowired
    private MessageRepository messageRepository;

    @GetMapping(value="{id}", produces={MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Message> getMessage(@PathVariable("id") int id) {
        System.out.println("GET");

        Message message = messageRepository.getMessageById(id);

        if(message != null)
            return new ResponseEntity<>(message, HttpStatus.OK);

        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping(consumes={"application/json", "application/xml"})
    public ResponseEntity<Message> addMessage(@Valid @RequestBody Message message, HttpServletRequest request) {

        if(message.getId() != 0)
            return ResponseEntity.badRequest().build();

        message = messageRepository.createMessage(message);
        URI uri = URI.create(request.getRequestURL() + "/" + message.getId());
        return ResponseEntity.created(uri).build();
    }

    @PutMapping(value="{id:^\\d+$}",
                consumes={"application/json", "application/xml"})
    public ResponseEntity<Message> updateMessage(@PathVariable("id") int id,
                                                 @Valid @RequestBody Message message) {

        if(message.getId() != id)
            return ResponseEntity.badRequest().build();

        messageRepository.updateMessage(message);
        return ResponseEntity.ok().build();
    }

    @PatchMapping(value="{id:^\\d+$}",
                  consumes={"application/json", "application/xml"},
                  produces={"application/json", "application/xml"})
    public ResponseEntity<Message> patchMessage(@PathVariable("id") int id,
                                                @RequestBody Message patchMessage) {
        System.out.println("PATCH");

        Message message = messageRepository.getMessageById(id);

        if(message == null)
            return ResponseEntity.notFound().build();

        if((patchMessage.getId() != 0) && (patchMessage.getId() != id))
            return ResponseEntity.badRequest().build();

        if(patchMessage.getAuthor() != null)
            message.setAuthor(patchMessage.getAuthor());

        if(patchMessage.getText() != null)
            message.setText(patchMessage.getText());

        message = messageRepository.updateMessage(message);
        return ResponseEntity.ok(message);
    }

    @DeleteMapping(value="{id:^\\d+$}")
    public ResponseEntity<Message> deleteMessage(@PathVariable("id") int id) {
        messageRepository.deleteMessage(id);
        return ResponseEntity.ok().build();
    }
}